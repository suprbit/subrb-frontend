import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity, updateEntity, createEntity, reset } from './feature.reducer';
import { IFeature } from 'app/shared/model/feature.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const FeatureUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const featureEntity = useAppSelector(state => state.feature.entity);
  const loading = useAppSelector(state => state.feature.loading);
  const updating = useAppSelector(state => state.feature.updating);
  const updateSuccess = useAppSelector(state => state.feature.updateSuccess);
  const handleClose = () => {
    props.history.push('/feature');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...featureEntity,
      ...values,
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...featureEntity,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="suprbFrontendApp.feature.home.createOrEditLabel" data-cy="FeatureCreateUpdateHeading">
            <Translate contentKey="suprbFrontendApp.feature.home.createOrEditLabel">Create or edit a Feature</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="feature-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField label={translate('suprbFrontendApp.feature.name')} id="feature-name" name="name" data-cy="name" type="text" />
              <ValidatedField
                label={translate('suprbFrontendApp.feature.isEnable')}
                id="feature-isEnable"
                name="isEnable"
                data-cy="isEnable"
                check
                type="checkbox"
              />
              <ValidatedField
                label={translate('suprbFrontendApp.feature.value')}
                id="feature-value"
                name="value"
                data-cy="value"
                type="text"
              />
              <ValidatedField label={translate('suprbFrontendApp.feature.key1')} id="feature-key1" name="key1" data-cy="key1" type="text" />
              <ValidatedField
                label={translate('suprbFrontendApp.feature.value1')}
                id="feature-value1"
                name="value1"
                data-cy="value1"
                type="text"
              />
              <ValidatedField label={translate('suprbFrontendApp.feature.key2')} id="feature-key2" name="key2" data-cy="key2" type="text" />
              <ValidatedField
                label={translate('suprbFrontendApp.feature.value2')}
                id="feature-value2"
                name="value2"
                data-cy="value2"
                type="text"
              />
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/feature" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default FeatureUpdate;

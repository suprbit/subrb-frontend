import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntities } from './feature-mapping.reducer';
import { IFeatureMapping } from 'app/shared/model/feature-mapping.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const FeatureMapping = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();

  const featureMappingList = useAppSelector(state => state.featureMapping.entities);
  const loading = useAppSelector(state => state.featureMapping.loading);

  useEffect(() => {
    dispatch(getEntities({}));
  }, []);

  const handleSyncList = () => {
    dispatch(getEntities({}));
  };

  const { match } = props;

  return (
    <div>
      <h2 id="feature-mapping-heading" data-cy="FeatureMappingHeading">
        <Translate contentKey="suprbFrontendApp.featureMapping.home.title">Feature Mappings</Translate>
        <div className="d-flex justify-content-end">
          <Button className="me-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="suprbFrontendApp.featureMapping.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="suprbFrontendApp.featureMapping.home.createLabel">Create new Feature Mapping</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {featureMappingList && featureMappingList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="suprbFrontendApp.featureMapping.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="suprbFrontendApp.featureMapping.isFeatureEnable">Is Feature Enable</Translate>
                </th>
                <th>
                  <Translate contentKey="suprbFrontendApp.featureMapping.targetType">Target Type</Translate>
                </th>
                <th>
                  <Translate contentKey="suprbFrontendApp.featureMapping.targetId">Target Id</Translate>
                </th>
                <th>
                  <Translate contentKey="suprbFrontendApp.featureMapping.priority">Priority</Translate>
                </th>
                <th>
                  <Translate contentKey="suprbFrontendApp.featureMapping.featureId">Feature Id</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {featureMappingList.map((featureMapping, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${featureMapping.id}`} color="link" size="sm">
                      {featureMapping.id}
                    </Button>
                  </td>
                  <td>{featureMapping.isFeatureEnable ? 'true' : 'false'}</td>
                  <td>
                    <Translate contentKey={`suprbFrontendApp.FeatureTargetType.${featureMapping.targetType}`} />
                  </td>
                  <td>{featureMapping.targetId}</td>
                  <td>{featureMapping.priority}</td>
                  <td>
                    {featureMapping.featureId ? (
                      <Link to={`feature/${featureMapping.featureId.id}`}>{featureMapping.featureId.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-end">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${featureMapping.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${featureMapping.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${featureMapping.id}/delete`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="suprbFrontendApp.featureMapping.home.notFound">No Feature Mappings found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default FeatureMapping;

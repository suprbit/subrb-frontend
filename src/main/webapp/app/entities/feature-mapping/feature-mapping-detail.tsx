import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './feature-mapping.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const FeatureMappingDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const featureMappingEntity = useAppSelector(state => state.featureMapping.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="featureMappingDetailsHeading">
          <Translate contentKey="suprbFrontendApp.featureMapping.detail.title">FeatureMapping</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{featureMappingEntity.id}</dd>
          <dt>
            <span id="isFeatureEnable">
              <Translate contentKey="suprbFrontendApp.featureMapping.isFeatureEnable">Is Feature Enable</Translate>
            </span>
          </dt>
          <dd>{featureMappingEntity.isFeatureEnable ? 'true' : 'false'}</dd>
          <dt>
            <span id="targetType">
              <Translate contentKey="suprbFrontendApp.featureMapping.targetType">Target Type</Translate>
            </span>
          </dt>
          <dd>{featureMappingEntity.targetType}</dd>
          <dt>
            <span id="targetId">
              <Translate contentKey="suprbFrontendApp.featureMapping.targetId">Target Id</Translate>
            </span>
          </dt>
          <dd>{featureMappingEntity.targetId}</dd>
          <dt>
            <span id="priority">
              <Translate contentKey="suprbFrontendApp.featureMapping.priority">Priority</Translate>
            </span>
          </dt>
          <dd>{featureMappingEntity.priority}</dd>
          <dt>
            <Translate contentKey="suprbFrontendApp.featureMapping.featureId">Feature Id</Translate>
          </dt>
          <dd>{featureMappingEntity.featureId ? featureMappingEntity.featureId.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/feature-mapping" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/feature-mapping/${featureMappingEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default FeatureMappingDetail;

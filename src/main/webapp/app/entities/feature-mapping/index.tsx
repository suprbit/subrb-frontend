import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import FeatureMapping from './feature-mapping';
import FeatureMappingDetail from './feature-mapping-detail';
import FeatureMappingUpdate from './feature-mapping-update';
import FeatureMappingDeleteDialog from './feature-mapping-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={FeatureMappingUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={FeatureMappingUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={FeatureMappingDetail} />
      <ErrorBoundaryRoute path={match.url} component={FeatureMapping} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={FeatureMappingDeleteDialog} />
  </>
);

export default Routes;

export interface IFeature {
  id?: number;
  name?: string | null;
  isEnable?: boolean | null;
  value?: string | null;
  key1?: string | null;
  value1?: string | null;
  key2?: string | null;
  value2?: string | null;
}

export const defaultValue: Readonly<IFeature> = {
  isEnable: false,
};

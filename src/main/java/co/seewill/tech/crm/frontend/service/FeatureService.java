package co.seewill.tech.crm.frontend.service;

import co.seewill.tech.crm.frontend.domain.Feature;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Feature}.
 */
public interface FeatureService {
    /**
     * Save a feature.
     *
     * @param feature the entity to save.
     * @return the persisted entity.
     */
    Feature save(Feature feature);

    /**
     * Partially updates a feature.
     *
     * @param feature the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Feature> partialUpdate(Feature feature);

    /**
     * Get all the features.
     *
     * @return the list of entities.
     */
    List<Feature> findAll();

    /**
     * Get the "id" feature.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Feature> findOne(Long id);

    /**
     * Delete the "id" feature.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

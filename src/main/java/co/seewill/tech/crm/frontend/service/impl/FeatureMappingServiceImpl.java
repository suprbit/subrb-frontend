package co.seewill.tech.crm.frontend.service.impl;

import co.seewill.tech.crm.frontend.domain.FeatureMapping;
import co.seewill.tech.crm.frontend.repository.FeatureMappingRepository;
import co.seewill.tech.crm.frontend.service.FeatureMappingService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link FeatureMapping}.
 */
@Service
@Transactional
public class FeatureMappingServiceImpl implements FeatureMappingService {

    private final Logger log = LoggerFactory.getLogger(FeatureMappingServiceImpl.class);

    private final FeatureMappingRepository featureMappingRepository;

    public FeatureMappingServiceImpl(FeatureMappingRepository featureMappingRepository) {
        this.featureMappingRepository = featureMappingRepository;
    }

    @Override
    public FeatureMapping save(FeatureMapping featureMapping) {
        log.debug("Request to save FeatureMapping : {}", featureMapping);
        return featureMappingRepository.save(featureMapping);
    }

    @Override
    public Optional<FeatureMapping> partialUpdate(FeatureMapping featureMapping) {
        log.debug("Request to partially update FeatureMapping : {}", featureMapping);

        return featureMappingRepository
            .findById(featureMapping.getId())
            .map(existingFeatureMapping -> {
                if (featureMapping.getIsFeatureEnable() != null) {
                    existingFeatureMapping.setIsFeatureEnable(featureMapping.getIsFeatureEnable());
                }
                if (featureMapping.getTargetType() != null) {
                    existingFeatureMapping.setTargetType(featureMapping.getTargetType());
                }
                if (featureMapping.getTargetId() != null) {
                    existingFeatureMapping.setTargetId(featureMapping.getTargetId());
                }
                if (featureMapping.getPriority() != null) {
                    existingFeatureMapping.setPriority(featureMapping.getPriority());
                }

                return existingFeatureMapping;
            })
            .map(featureMappingRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<FeatureMapping> findAll() {
        log.debug("Request to get all FeatureMappings");
        return featureMappingRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<FeatureMapping> findOne(Long id) {
        log.debug("Request to get FeatureMapping : {}", id);
        return featureMappingRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete FeatureMapping : {}", id);
        featureMappingRepository.deleteById(id);
    }
}

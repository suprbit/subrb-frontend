package co.seewill.tech.crm.frontend.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * A Campaign.
 */
@Entity
@Table(name = "campaign")
public class Campaign implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "campaign_name")
    private String campaignName;

    @Column(name = "campaign_url")
    private String campaignUrl;

    @Column(name = "campaign_type")
    private String campaignType;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Campaign id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCampaignName() {
        return this.campaignName;
    }

    public Campaign campaignName(String campaignName) {
        this.setCampaignName(campaignName);
        return this;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getCampaignUrl() {
        return this.campaignUrl;
    }

    public Campaign campaignUrl(String campaignUrl) {
        this.setCampaignUrl(campaignUrl);
        return this;
    }

    public void setCampaignUrl(String campaignUrl) {
        this.campaignUrl = campaignUrl;
    }

    public String getCampaignType() {
        return this.campaignType;
    }

    public Campaign campaignType(String campaignType) {
        this.setCampaignType(campaignType);
        return this;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Campaign)) {
            return false;
        }
        return id != null && id.equals(((Campaign) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Campaign{" +
            "id=" + getId() +
            ", campaignName='" + getCampaignName() + "'" +
            ", campaignUrl='" + getCampaignUrl() + "'" +
            ", campaignType='" + getCampaignType() + "'" +
            "}";
    }
}

package co.seewill.tech.crm.frontend.service.impl;

import co.seewill.tech.crm.frontend.domain.Feature;
import co.seewill.tech.crm.frontend.repository.FeatureRepository;
import co.seewill.tech.crm.frontend.service.FeatureService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Feature}.
 */
@Service
@Transactional
public class FeatureServiceImpl implements FeatureService {

    private final Logger log = LoggerFactory.getLogger(FeatureServiceImpl.class);

    private final FeatureRepository featureRepository;

    public FeatureServiceImpl(FeatureRepository featureRepository) {
        this.featureRepository = featureRepository;
    }

    @Override
    public Feature save(Feature feature) {
        log.debug("Request to save Feature : {}", feature);
        return featureRepository.save(feature);
    }

    @Override
    public Optional<Feature> partialUpdate(Feature feature) {
        log.debug("Request to partially update Feature : {}", feature);

        return featureRepository
            .findById(feature.getId())
            .map(existingFeature -> {
                if (feature.getName() != null) {
                    existingFeature.setName(feature.getName());
                }
                if (feature.getIsEnable() != null) {
                    existingFeature.setIsEnable(feature.getIsEnable());
                }
                if (feature.getValue() != null) {
                    existingFeature.setValue(feature.getValue());
                }
                if (feature.getKey1() != null) {
                    existingFeature.setKey1(feature.getKey1());
                }
                if (feature.getValue1() != null) {
                    existingFeature.setValue1(feature.getValue1());
                }
                if (feature.getKey2() != null) {
                    existingFeature.setKey2(feature.getKey2());
                }
                if (feature.getValue2() != null) {
                    existingFeature.setValue2(feature.getValue2());
                }

                return existingFeature;
            })
            .map(featureRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Feature> findAll() {
        log.debug("Request to get all Features");
        return featureRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Feature> findOne(Long id) {
        log.debug("Request to get Feature : {}", id);
        return featureRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Feature : {}", id);
        featureRepository.deleteById(id);
    }
}

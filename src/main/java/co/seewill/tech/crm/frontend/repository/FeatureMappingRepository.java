package co.seewill.tech.crm.frontend.repository;

import co.seewill.tech.crm.frontend.domain.FeatureMapping;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the FeatureMapping entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FeatureMappingRepository extends JpaRepository<FeatureMapping, Long> {}

/**
 * View Models used by Spring MVC REST controllers.
 */
package co.seewill.tech.crm.frontend.web.rest.vm;

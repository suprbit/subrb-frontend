/**
 * Data Access Objects used by WebSocket services.
 */
package co.seewill.tech.crm.frontend.web.websocket.dto;

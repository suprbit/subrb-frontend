package co.seewill.tech.crm.frontend.web.rest;

import co.seewill.tech.crm.frontend.domain.FeatureMapping;
import co.seewill.tech.crm.frontend.repository.FeatureMappingRepository;
import co.seewill.tech.crm.frontend.service.FeatureMappingService;
import co.seewill.tech.crm.frontend.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link co.seewill.tech.crm.frontend.domain.FeatureMapping}.
 */
@RestController
@RequestMapping("/api")
public class FeatureMappingResource {

    private final Logger log = LoggerFactory.getLogger(FeatureMappingResource.class);

    private static final String ENTITY_NAME = "featureMapping";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FeatureMappingService featureMappingService;

    private final FeatureMappingRepository featureMappingRepository;

    public FeatureMappingResource(FeatureMappingService featureMappingService, FeatureMappingRepository featureMappingRepository) {
        this.featureMappingService = featureMappingService;
        this.featureMappingRepository = featureMappingRepository;
    }

    /**
     * {@code POST  /feature-mappings} : Create a new featureMapping.
     *
     * @param featureMapping the featureMapping to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new featureMapping, or with status {@code 400 (Bad Request)} if the featureMapping has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/feature-mappings")
    public ResponseEntity<FeatureMapping> createFeatureMapping(@RequestBody FeatureMapping featureMapping) throws URISyntaxException {
        log.debug("REST request to save FeatureMapping : {}", featureMapping);
        if (featureMapping.getId() != null) {
            throw new BadRequestAlertException("A new featureMapping cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FeatureMapping result = featureMappingService.save(featureMapping);
        return ResponseEntity
            .created(new URI("/api/feature-mappings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /feature-mappings/:id} : Updates an existing featureMapping.
     *
     * @param id the id of the featureMapping to save.
     * @param featureMapping the featureMapping to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated featureMapping,
     * or with status {@code 400 (Bad Request)} if the featureMapping is not valid,
     * or with status {@code 500 (Internal Server Error)} if the featureMapping couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/feature-mappings/{id}")
    public ResponseEntity<FeatureMapping> updateFeatureMapping(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody FeatureMapping featureMapping
    ) throws URISyntaxException {
        log.debug("REST request to update FeatureMapping : {}, {}", id, featureMapping);
        if (featureMapping.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, featureMapping.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!featureMappingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        FeatureMapping result = featureMappingService.save(featureMapping);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, featureMapping.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /feature-mappings/:id} : Partial updates given fields of an existing featureMapping, field will ignore if it is null
     *
     * @param id the id of the featureMapping to save.
     * @param featureMapping the featureMapping to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated featureMapping,
     * or with status {@code 400 (Bad Request)} if the featureMapping is not valid,
     * or with status {@code 404 (Not Found)} if the featureMapping is not found,
     * or with status {@code 500 (Internal Server Error)} if the featureMapping couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/feature-mappings/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<FeatureMapping> partialUpdateFeatureMapping(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody FeatureMapping featureMapping
    ) throws URISyntaxException {
        log.debug("REST request to partial update FeatureMapping partially : {}, {}", id, featureMapping);
        if (featureMapping.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, featureMapping.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!featureMappingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<FeatureMapping> result = featureMappingService.partialUpdate(featureMapping);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, featureMapping.getId().toString())
        );
    }

    /**
     * {@code GET  /feature-mappings} : get all the featureMappings.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of featureMappings in body.
     */
    @GetMapping("/feature-mappings")
    public List<FeatureMapping> getAllFeatureMappings() {
        log.debug("REST request to get all FeatureMappings");
        return featureMappingService.findAll();
    }

    /**
     * {@code GET  /feature-mappings/:id} : get the "id" featureMapping.
     *
     * @param id the id of the featureMapping to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the featureMapping, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/feature-mappings/{id}")
    public ResponseEntity<FeatureMapping> getFeatureMapping(@PathVariable Long id) {
        log.debug("REST request to get FeatureMapping : {}", id);
        Optional<FeatureMapping> featureMapping = featureMappingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(featureMapping);
    }

    /**
     * {@code DELETE  /feature-mappings/:id} : delete the "id" featureMapping.
     *
     * @param id the id of the featureMapping to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/feature-mappings/{id}")
    public ResponseEntity<Void> deleteFeatureMapping(@PathVariable Long id) {
        log.debug("REST request to delete FeatureMapping : {}", id);
        featureMappingService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

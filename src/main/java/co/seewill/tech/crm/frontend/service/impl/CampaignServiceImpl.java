package co.seewill.tech.crm.frontend.service.impl;

import co.seewill.tech.crm.frontend.domain.Campaign;
import co.seewill.tech.crm.frontend.repository.CampaignRepository;
import co.seewill.tech.crm.frontend.service.CampaignService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Campaign}.
 */
@Service
@Transactional
public class CampaignServiceImpl implements CampaignService {

    private final Logger log = LoggerFactory.getLogger(CampaignServiceImpl.class);

    private final CampaignRepository campaignRepository;

    public CampaignServiceImpl(CampaignRepository campaignRepository) {
        this.campaignRepository = campaignRepository;
    }

    @Override
    public Campaign save(Campaign campaign) {
        log.debug("Request to save Campaign : {}", campaign);
        return campaignRepository.save(campaign);
    }

    @Override
    public Optional<Campaign> partialUpdate(Campaign campaign) {
        log.debug("Request to partially update Campaign : {}", campaign);

        return campaignRepository
            .findById(campaign.getId())
            .map(existingCampaign -> {
                if (campaign.getCampaignName() != null) {
                    existingCampaign.setCampaignName(campaign.getCampaignName());
                }
                if (campaign.getCampaignUrl() != null) {
                    existingCampaign.setCampaignUrl(campaign.getCampaignUrl());
                }
                if (campaign.getCampaignType() != null) {
                    existingCampaign.setCampaignType(campaign.getCampaignType());
                }

                return existingCampaign;
            })
            .map(campaignRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Campaign> findAll(Pageable pageable) {
        log.debug("Request to get all Campaigns");
        return campaignRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Campaign> findOne(Long id) {
        log.debug("Request to get Campaign : {}", id);
        return campaignRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Campaign : {}", id);
        campaignRepository.deleteById(id);
    }
}

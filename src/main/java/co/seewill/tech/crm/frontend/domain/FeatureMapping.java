package co.seewill.tech.crm.frontend.domain;

import co.seewill.tech.crm.frontend.domain.enumeration.FeatureTargetType;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A FeatureMapping.
 */
@Entity
@Table(name = "feature_mapping")
public class FeatureMapping implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "is_feature_enable")
    private Boolean isFeatureEnable;

    @Enumerated(EnumType.STRING)
    @Column(name = "target_type")
    private FeatureTargetType targetType;

    @Column(name = "target_id")
    private Long targetId;

    @Column(name = "priority")
    private Double priority;

    @OneToOne
    @JoinColumn(unique = true)
    private Feature featureId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public FeatureMapping id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getIsFeatureEnable() {
        return this.isFeatureEnable;
    }

    public FeatureMapping isFeatureEnable(Boolean isFeatureEnable) {
        this.setIsFeatureEnable(isFeatureEnable);
        return this;
    }

    public void setIsFeatureEnable(Boolean isFeatureEnable) {
        this.isFeatureEnable = isFeatureEnable;
    }

    public FeatureTargetType getTargetType() {
        return this.targetType;
    }

    public FeatureMapping targetType(FeatureTargetType targetType) {
        this.setTargetType(targetType);
        return this;
    }

    public void setTargetType(FeatureTargetType targetType) {
        this.targetType = targetType;
    }

    public Long getTargetId() {
        return this.targetId;
    }

    public FeatureMapping targetId(Long targetId) {
        this.setTargetId(targetId);
        return this;
    }

    public void setTargetId(Long targetId) {
        this.targetId = targetId;
    }

    public Double getPriority() {
        return this.priority;
    }

    public FeatureMapping priority(Double priority) {
        this.setPriority(priority);
        return this;
    }

    public void setPriority(Double priority) {
        this.priority = priority;
    }

    public Feature getFeatureId() {
        return this.featureId;
    }

    public void setFeatureId(Feature feature) {
        this.featureId = feature;
    }

    public FeatureMapping featureId(Feature feature) {
        this.setFeatureId(feature);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FeatureMapping)) {
            return false;
        }
        return id != null && id.equals(((FeatureMapping) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FeatureMapping{" +
            "id=" + getId() +
            ", isFeatureEnable='" + getIsFeatureEnable() + "'" +
            ", targetType='" + getTargetType() + "'" +
            ", targetId=" + getTargetId() +
            ", priority=" + getPriority() +
            "}";
    }
}

package co.seewill.tech.crm.frontend.domain.enumeration;

/**
 * The FeatureTargetType enumeration.
 */
public enum FeatureTargetType {
    USER,
}

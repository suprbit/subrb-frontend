import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class FeatureMappingUpdatePage {
  pageTitle: ElementFinder = element(by.id('suprbFrontendApp.featureMapping.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  isFeatureEnableInput: ElementFinder = element(by.css('input#feature-mapping-isFeatureEnable'));
  targetTypeSelect: ElementFinder = element(by.css('select#feature-mapping-targetType'));
  targetIdInput: ElementFinder = element(by.css('input#feature-mapping-targetId'));
  priorityInput: ElementFinder = element(by.css('input#feature-mapping-priority'));
  featureIdSelect: ElementFinder = element(by.css('select#feature-mapping-featureId'));

  getPageTitle() {
    return this.pageTitle;
  }

  getIsFeatureEnableInput() {
    return this.isFeatureEnableInput;
  }
  async setTargetTypeSelect(targetType) {
    await this.targetTypeSelect.sendKeys(targetType);
  }

  async getTargetTypeSelect() {
    return this.targetTypeSelect.element(by.css('option:checked')).getText();
  }

  async targetTypeSelectLastOption() {
    await this.targetTypeSelect.all(by.tagName('option')).last().click();
  }
  async setTargetIdInput(targetId) {
    await this.targetIdInput.sendKeys(targetId);
  }

  async getTargetIdInput() {
    return this.targetIdInput.getAttribute('value');
  }

  async setPriorityInput(priority) {
    await this.priorityInput.sendKeys(priority);
  }

  async getPriorityInput() {
    return this.priorityInput.getAttribute('value');
  }

  async featureIdSelectLastOption() {
    await this.featureIdSelect.all(by.tagName('option')).last().click();
  }

  async featureIdSelectOption(option) {
    await this.featureIdSelect.sendKeys(option);
  }

  getFeatureIdSelect() {
    return this.featureIdSelect;
  }

  async getFeatureIdSelectedOption() {
    return this.featureIdSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    const selectedIsFeatureEnable = await this.getIsFeatureEnableInput().isSelected();
    if (selectedIsFeatureEnable) {
      await this.getIsFeatureEnableInput().click();
    } else {
      await this.getIsFeatureEnableInput().click();
    }
    await waitUntilDisplayed(this.saveButton);
    await this.targetTypeSelectLastOption();
    await waitUntilDisplayed(this.saveButton);
    await this.setTargetIdInput('5');
    await waitUntilDisplayed(this.saveButton);
    await this.setPriorityInput('5');
    await this.featureIdSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
  }
}

import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class CampaignUpdatePage {
  pageTitle: ElementFinder = element(by.id('suprbFrontendApp.campaign.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  campaignNameInput: ElementFinder = element(by.css('input#campaign-campaignName'));
  campaignUrlInput: ElementFinder = element(by.css('input#campaign-campaignUrl'));
  campaignTypeInput: ElementFinder = element(by.css('input#campaign-campaignType'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setCampaignNameInput(campaignName) {
    await this.campaignNameInput.sendKeys(campaignName);
  }

  async getCampaignNameInput() {
    return this.campaignNameInput.getAttribute('value');
  }

  async setCampaignUrlInput(campaignUrl) {
    await this.campaignUrlInput.sendKeys(campaignUrl);
  }

  async getCampaignUrlInput() {
    return this.campaignUrlInput.getAttribute('value');
  }

  async setCampaignTypeInput(campaignType) {
    await this.campaignTypeInput.sendKeys(campaignType);
  }

  async getCampaignTypeInput() {
    return this.campaignTypeInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setCampaignNameInput('campaignName');
    await waitUntilDisplayed(this.saveButton);
    await this.setCampaignUrlInput('campaignUrl');
    await waitUntilDisplayed(this.saveButton);
    await this.setCampaignTypeInput('campaignType');
    await this.save();
    await waitUntilHidden(this.saveButton);
  }
}

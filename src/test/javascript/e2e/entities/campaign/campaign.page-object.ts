import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import CampaignUpdatePage from './campaign-update.page-object';

const expect = chai.expect;
export class CampaignDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('suprbFrontendApp.campaign.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-campaign'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class CampaignComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('campaign-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('campaign');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateCampaign() {
    await this.createButton.click();
    return new CampaignUpdatePage();
  }

  async deleteCampaign() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const campaignDeleteDialog = new CampaignDeleteDialog();
    await waitUntilDisplayed(campaignDeleteDialog.deleteModal);
    expect(await campaignDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/suprbFrontendApp.campaign.delete.question/);
    await campaignDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(campaignDeleteDialog.deleteModal);

    expect(await isVisible(campaignDeleteDialog.deleteModal)).to.be.false;
  }
}

import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class ProjectUpdatePage {
  pageTitle: ElementFinder = element(by.id('suprbFrontendApp.project.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  projectNameInput: ElementFinder = element(by.css('input#project-projectName'));
  projectUrlInput: ElementFinder = element(by.css('input#project-projectUrl'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setProjectNameInput(projectName) {
    await this.projectNameInput.sendKeys(projectName);
  }

  async getProjectNameInput() {
    return this.projectNameInput.getAttribute('value');
  }

  async setProjectUrlInput(projectUrl) {
    await this.projectUrlInput.sendKeys(projectUrl);
  }

  async getProjectUrlInput() {
    return this.projectUrlInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setProjectNameInput('projectName');
    await waitUntilDisplayed(this.saveButton);
    await this.setProjectUrlInput('projectUrl');
    await this.save();
    await waitUntilHidden(this.saveButton);
  }
}

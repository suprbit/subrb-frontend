package co.seewill.tech.crm.frontend.domain;

import static org.assertj.core.api.Assertions.assertThat;

import co.seewill.tech.crm.frontend.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class FeatureMappingTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FeatureMapping.class);
        FeatureMapping featureMapping1 = new FeatureMapping();
        featureMapping1.setId(1L);
        FeatureMapping featureMapping2 = new FeatureMapping();
        featureMapping2.setId(featureMapping1.getId());
        assertThat(featureMapping1).isEqualTo(featureMapping2);
        featureMapping2.setId(2L);
        assertThat(featureMapping1).isNotEqualTo(featureMapping2);
        featureMapping1.setId(null);
        assertThat(featureMapping1).isNotEqualTo(featureMapping2);
    }
}

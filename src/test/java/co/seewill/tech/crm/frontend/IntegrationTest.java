package co.seewill.tech.crm.frontend;

import co.seewill.tech.crm.frontend.SuprbFrontendApp;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Base composite annotation for integration tests.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@SpringBootTest(classes = SuprbFrontendApp.class)
public @interface IntegrationTest {
}
